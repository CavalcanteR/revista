// Initialize your app
var myApp = new Framework7({
    animateNavBackIcon: true,
    // Enable templates auto precompilation
    precompileTemplates: true,
    // Enabled pages rendering using Template7
	swipeBackPage: true,
	pushState: true,
    template7Pages: true
});

// Export selectors engine
var $$ = Dom7;

// Add main View
var mainView = myApp.addView('.view-main', {
    // Enable dynamic Navbar
    dynamicNavbar: false,
});



var mesAtual;
function carregarNoticias() {
	var paginaAtual = 0;
//alert('entrou');
	$.ajax({
		url: 'http://revistacondominio.com.br/index.php/noticias-recentes/categoria-de-noticias?format=feed',
		dataType: 'json',
		beforeSend: function() {
			$("#template_post").html('Carregando...');
		},
		success : function(noticias) {
			var postHTML;
			for( item in noticias ) {
				var post = noticias[item].title;
//alert(post);
//				postHTML +='<li class="swipeout">';
//				postHTML +='  <div class="swipeout-content item-content">';
//				postHTML +='	<div class="post_entry">';
//				postHTML +='		<div class="post_thumb"><img src="http://www.ricardocavalcante.com/Revista/noticias/'+post.imagem+'" alt="" title="" /></div>';
//				postHTML +='		<div class="post_details">';
//				postHTML +='		<h2><a onclick="carregarDetalhe('+post.id+')" href="blog-single.html?id='+post.id+'">'+post.titulo+'</a></h2>';
//				postHTML +='		<p>'+post.resumo+'</p>';
//				postHTML +='		</div>';
//				postHTML +='		<div class="post_swipe"><img src="images/swipe_more.png" alt="" title="" /></div>';
//				postHTML +='	</div>';
//				postHTML +='  </div>';
//				postHTML +='  <div class="swipeout-actions-right">';
//				postHTML +='	<a href="#" class="action1 open-popup" data-popup=".popup-social"><img src="images/icons/white/heart.png" alt="" title="" /></a>';
//				postHTML +='  </div>';
//				postHTML +='</li>';
			}
			$("#template_post").html(postHTML);
			paginaAtual++;
		}
	});
}

function carregarFornecedores(cidade, tipo) {
	$.ajax({
		url: 'http://www.ricardocavalcante.com/Revista/indexFornecedores.html',
		data: {
			cidade:cidade, categoria:tipo
		},
		dataType: 'json',
		beforeSend: function() {
		},
		success : function(noticias) {
			var postHTML = '';
			for( mes in noticias ) {
				for( index in noticias[mes] ) {
					var post = noticias[mes][index];
					postHTML +='<li class="swipeout">';
					postHTML +='  <div class="swipeout-content item-content">';
					postHTML +='	<div class="post_entry">';
					postHTML +='		<div class="post_thumb"><img src="http://www.ricardocavalcante.com/Revista/noticias/'+post.imagem+'" alt="" title="" /></div>';
					postHTML +='		<div class="post_details">';
					postHTML +='		<h2><a onclick="carregarDetalhe('+post.id+')" href="blog-single.html?id='+post.id+'">'+post.titulo+'</a></h2>';
					postHTML +='		<p>'+post.texto+'</p>';
					postHTML +='		</div>';
					postHTML +='		<div class="post_swipe"><img src="images/swipe_more.png" alt="" title="" /></div>';
					postHTML +='	</div>';
					postHTML +='  </div>';
					postHTML +='  <div class="swipeout-actions-right">';
					postHTML +='	<a href="#" class="action1 open-popup" data-popup=".popup-social"><img src="images/icons/white/heart.png" alt="" title="" /></a>';
					postHTML +='  </div>';
					postHTML +='</li>';
				}
			}
			if(postHTML=='') { 
				$("#template_fornecedores").html('<li>Nada encontrado</li>');
			} else {
				$("#template_fornecedores").html(postHTML);
			}
		}
	});
}

function carregarDetalhe(id) {
	if(id>0) {
		$.ajax({
			url: 'http://www.ricardocavalcante.com/Revista/indexDetalhe/'+id+'.html',
			data: {  },
			dataType: 'json',
			beforeSend: function() {
			},
			success : function(noticias) {
				var postHTML = '';
				for( post in noticias ) {
						postHTML +='<img src="http://www.ricardocavalcante.com/Revista/noticias/'+noticias[post].imagem+'" alt="" title="" />';
						postHTML +='<p>';
						postHTML += noticias[post].texto;
						postHTML +='</p>';

						tituloNoticia = noticias[post].titulo;
				}
				$("#corpo_noticia").html(postHTML);
				$("#titulo_noticia").html(tituloNoticia);
			}
		});
	}

}

function carregarTipos() {
	$.ajax({
		url: 'http://www.ricardocavalcante.com/Revista/indexListaCategorias.html',
		data: {  },
		dataType: 'json',
		beforeSend: function() {
		},
		success : function(noticias) {
			var postHTML = '<option value="0" >Selecione</option>';
			for( post in noticias ) {
					postHTML +='<option value="'+noticias[post].id+'" >';
					postHTML += noticias[post].nome;
					postHTML +='</option>';
			}
			$("#listaTipo").html(postHTML);
		}
	});
}

function carregarPropagandas() {
	$.ajax({
		url: 'http://www.ricardocavalcante.com/Revista/indexListaPropagangas.html',
		data: {  },
		dataType: 'json',
		beforeSend: function() {
		},
		success : function(noticias) {
			var listaImagens = '';
			for( post in noticias ) {
				listaImagens += '<a class="imgPropaganda" href="'+noticias[post].titulo_2+'" target="_blank" >';
				listaImagens += '<img  src="http://www.ricardocavalcante.com/Revista/painel/'+noticias[post].imagem+'" alt="" title="" />';
				listaImagens += '</a>';
			}
			$("#propagandas").html(listaImagens);

			carousel();
		}
	});
}

carregarPropagandas();

var slideIndex = 0;
function carousel() {
    var i;
    var x = document.getElementsByClassName("imgPropaganda");
    for (i = 0; i < x.length; i++) {
      x[i].style.display = "none";
    }
    slideIndex++;
    if (slideIndex > x.length) {slideIndex = 1}
    x[slideIndex-1].style.display = "block";
    setTimeout(carousel, 5000); // Change image every 2 seconds
}

$$(document).on('pageInit', function (e) {


  		$(".swipebox").swipebox();
		$(".videocontainer").fitVids();
		
		$("#ContactForm").validate({
			submitHandler: function(form) {
				ajaxContact(form);
				return false;
			}
		});
		

		$('#loadMore').click(function () {
			carregarNoticias();
		
		});

		//$('#carregaFornecedores').click(function () {
		//	carregarFornecedores();
		
		//});

		carregarNoticias();
		carregarTipos();
		carregarPropagandas();
		
	$("a.switcher").bind("click", function(e){
		e.preventDefault();
		
		var theid = $(this).attr("id");
		var theproducts = $("ul#photoslist");
		var classNames = $(this).attr('class').split(' ');
		
		
		if($(this).hasClass("active")) {
			// if currently clicked button has the active class
			// then we do nothing!
			return false;
		} else {
			// otherwise we are clicking on the inactive button
			// and in the process of switching views!

  			if(theid == "view13") {
				$(this).addClass("active");
				$("#view11").removeClass("active");
				$("#view11").children("img").attr("src","images/switch_11.png");
				
				$("#view12").removeClass("active");
				$("#view12").children("img").attr("src","images/switch_12.png");
			
				var theimg = $(this).children("img");
				theimg.attr("src","images/switch_13_active.png");
			
				// remove the list class and change to grid
				theproducts.removeClass("photo_gallery_11");
				theproducts.removeClass("photo_gallery_12");
				theproducts.addClass("photo_gallery_13");

			}
			
			else if(theid == "view12") {
				$(this).addClass("active");
				$("#view11").removeClass("active");
				$("#view11").children("img").attr("src","images/switch_11.png");
				
				$("#view13").removeClass("active");
				$("#view13").children("img").attr("src","images/switch_13.png");
			
				var theimg = $(this).children("img");
				theimg.attr("src","images/switch_12_active.png");
			
				// remove the list class and change to grid
				theproducts.removeClass("photo_gallery_11");
				theproducts.removeClass("photo_gallery_13");
				theproducts.addClass("photo_gallery_12");

			} 
			else if(theid == "view11") {
				$("#view12").removeClass("active");
				$("#view12").children("img").attr("src","images/switch_12.png");
				
				$("#view13").removeClass("active");
				$("#view13").children("img").attr("src","images/switch_13.png");
			
				var theimg = $(this).children("img");
				theimg.attr("src","images/switch_11_active.png");
			
				// remove the list class and change to grid
				theproducts.removeClass("photo_gallery_12");
				theproducts.removeClass("photo_gallery_13");
				theproducts.addClass("photo_gallery_11");

			} 
			
		}


	});	
	
	document.addEventListener('touchmove', function(event) {
	   if(event.target.parentNode.className.indexOf('navbarpages') != -1 || event.target.className.indexOf('navbarpages') != -1 ) {
		event.preventDefault(); }
	}, false);
	
	// Add ScrollFix
	var scrollingContent = document.getElementById("pages_maincontent");
	new ScrollFix(scrollingContent);
	
	
	var ScrollFix = function(elem) {
		// Variables to track inputs
		var startY = startTopScroll = deltaY = undefined,
	
		elem = elem || elem.querySelector(elem);
	
		// If there is no element, then do nothing	
		if(!elem)
			return;
	
		// Handle the start of interactions
		elem.addEventListener('touchstart', function(event){
			startY = event.touches[0].pageY;
			startTopScroll = elem.scrollTop;
	
			if(startTopScroll <= 0)
				elem.scrollTop = 1;
	
			if(startTopScroll + elem.offsetHeight >= elem.scrollHeight)
				elem.scrollTop = elem.scrollHeight - elem.offsetHeight - 1;
		}, false);
	};
	
		
		
})
